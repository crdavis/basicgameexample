package com.mycompany.basicgameexample;

import com.almasb.fxgl.app.GameApplication;
import com.almasb.fxgl.app.GameSettings;
import com.almasb.fxgl.dsl.FXGL;
import com.almasb.fxgl.entity.Entity;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import java.util.Map;

import static com.almasb.fxgl.dsl.FXGL.*;

/**
 *
 * @author Carlton Davis
 */
//Note GameApplication class is extended, not Application class
public class BasicGameApp extends GameApplication {

    //Set up the game window
    @Override
    protected void initSettings(GameSettings settings) {
        settings.setWidth(700);
        settings.setHeight(700);
        settings.setTitle("Basic Game App");
        settings.setVersion("0.1");
    }

    //Create Game play variable
    @Override
    protected void initGameVars(Map<String, Object> vars) {
        vars.put("pixelsMoved", 0);
    }

    // Class field for player.
    private Entity player;

    // Add a player to the screen. The player is a blue circle
    @Override
    protected void initGame() {
        player = FXGL.entityBuilder()
                .at(300, 300)
                .view(new Circle(300.0f, 300.0f, 15, Color.BLUE))
                .buildAndAttach();
    }

    //Setup input handling code
    @Override
    protected void initInput() {
        onKey(KeyCode.RIGHT, () -> {
            player.translateX(5); // move right 5 pixels
            inc("pixelsMoved", +5);
        });

        onKey(KeyCode.LEFT, () -> {
            player.translateX(-5); // move left 5 pixels
            inc("pixelsMoved", -5);
        });

        onKey(KeyCode.UP, () -> {
            player.translateY(-5); // move up 5 pixels
            inc("pixelsMoved", +5);
        });

        onKey(KeyCode.DOWN, () -> {
            player.translateY(5); // move down 5 pixels
            inc("pixelsMoved", +5);
        });
    }
    
    //Setup UI for the game
    @Override
    protected void initUI() {
        Text textPixels = new Text();
        textPixels.setTranslateX(50); // x = 50
        textPixels.setTranslateY(100); // y = 100
        
        /*Bind the UI text object to the variable pixelsMoved. This allows
          the UI text to automatically show the number of pixels the player
          has moved.
        */
        textPixels.textProperty().bind(getWorldProperties().intProperty("pixelsMoved").asString());
        
        //Add the UI node to the scene graph
        getGameScene().addUINode(textPixels);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
